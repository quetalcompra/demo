package com.quetalcompras.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

	@GetMapping("/hello-world")
	public String helloWorld() {
		return "Hello World!";
	}
	
	@PostMapping("/hello-world")
	public Articulo updateHelloWorld() {
		Articulo dron = new Articulo();
		dron.setId(1);
		dron.setNombre("DJI 1");
		dron.setDescripcion("Dron de agricultura");
		return dron;
	}
}
